# simple whois

Get short information about IP address from WHOIS database.

### usage

```
$ARG1 [IP] # return basic information about IP address

-c, --csv  # return output in CSV
-h, --help # show usage

Example:
./ip_info 8.8.8.8
./ip_info -c 8.8.8.8
```