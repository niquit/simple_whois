FROM ubuntu:16.04
RUN apt-get update
RUN apt-get install -y curl
COPY bin/ip_info ip_info
ENTRYPOINT ["./ip_info"]
CMD ["185.33.37.131"]